--Debug
function DebugFormat(string,hex) -- Format strings containing non-printable characters so we can see what they are
    local visual = {}
    local format = hex~=nil and "\\x%02x" or "[%02X]"
    for i=1,#string do
      local byte = string:sub(i,i)
      if string.byte(byte) >= 0x20 and string.byte(byte) <= 0x7E then -- printable ASCII
        table.insert(visual,byte)
      else -- non-printable ASCII
        table.insert(visual,string.format(format,string.byte(byte)))
      end
    end
    return table.concat(visual)
  end
  --Ascii Conversion
  function convertAscii(hexstring)
    hexstring = string.gsub(hexstring, "%s+", "")
    asciistring = bitstring.fromhexstream(hexstring)
    return DebugFormat(asciistring)
  end
  
  --command variables
  crlf = '\x0d\x0a'
  -----Testing-----
  getMonName = '\x37\x51\x02\xEB\x01\x8E'..crlf
  getMonSerial = '\x37\x51\x02\xEB\x02\x8D'..crlf
  -----Set Power-----
  --pwrOff = '\x37\x51\x03\xEA\x20\x00'..crlf Losing Network Connection, use instead:
  pwrStandby = '\x37\x51\x03\xEA\x20\x02\xAD'..crlf
  pwrOn = '\x37\x51\x03\xEA\x20\x01'..crlf
  -----Set Input-----
  hdmi1 = '\x37\x51\x06\xEA\x62\x01\x00\x00\x00\xE9'..crlf
  dp1 = '\x37\x51\x06\xEA\x62\x08\x00\x00\x00\xE0'..crlf
  -----Get Power-----
  getPwr = '\x37\x51\x02\xEB\x20\xAF'..crlf
      --getPwrStateOff = '\x0020o7\x0003\x0002'
  getPwrStateStandby = '\x006f\x0037\x0004\x0029'
  getPwrStateOn = '\x006f\x0037\x0004\x0029'
  
  -----Get Input-----
  getInput = ''
    getInputStateHdmi1 = ''..crlf
    getInputStateDp1 = ''..crlf
  
  
  --tcp variables + socket info
  address = Controls.ipAddress.String
  port = 4661
  sock = TcpSocket.New()
  sock.ReadTimeout = 0
  sock.WriteTimeout = 0
  sock.ReconnectTimeout = 5
  
  --tcp socket
  sock.Connected = function(sock)
    print("TCP socket is connected")
  end
  sock.Reconnect = function(sock)
    print("TCP socket is reconnecting")
  end
  sock.Closed = function(sock)
    print("TCP socket was closed by the remote end")
  end
  sock.Error = function(sock, err)
    print("TCP socket had an error:",err)
  end
  sock.Timeout = function(sock, err)
    print("TCP socket timed out",err)
  end
  
  --keep alive
  statusPoll = function()
    print('polling...')
    sock:Write(getPwr)
    --sock:Write(getInput)
    Timer.CallAfter(statusPoll, 10)
  end
  Timer.CallAfter(statusPoll, 5)
  
  --button state functions
  pwrIsOff = function()
    print('Turning Display Off')
    Controls.pwrState.Color = 'Red'
    Controls.pwrState.Legend = 'Off'
  end
  pwrIsOn = function()
    print('Turning Display On')
    Controls.pwrState.Color = 'Green'
    Controls.pwrState.Legend = 'On'
  end
  inputIsDp1 = function()
    print('Changing to DP 1')
    Controls.inputState.Color = 'Yellow'
    Controls.inputState.Legend = 'DP 1'
  end
  inputIsHdmi1 = function()
    print('Changing to HDMI 1')
    Controls.inputState.Color = 'Orange'
    Controls.inputState.Legend = 'HDMI 1'
  end
  
  --write functions
  Controls.pwrOff.EventHandler = function()
    sock:Write(pwrStandby)
    --pwrIsOff()
    print('Sent: '..pwrStandby)
  end
  Controls.pwrOn.EventHandler = function()
    sock:Write(pwrOn)
    --pwrIsOn()
    print('Sent: '..pwrOn)
  end
  Controls.hdmi1.EventHandler = function() 
    sock:Write(hdmi1)
    inputIsHdmi1()
    print('Sent: '..hdmi1)
  end
  Controls.dp1.EventHandler = function()
    sock:Write(dp1)
    inputIsDp1()
    print('Sent: '..dp1)
  end
  
  --read functions
  sock.Data = function(sock)
    raw = sock:Read(sock.BufferLength)
    formatted = DebugFormat(raw)
    print('raw: '..raw)
    print('formatted: '..formatted)
    feedback()
    powerState()
    --inputState()
  end
  --display feedback
  function feedback()
    if Fb ~= nil then
      Controls.dispFB.String = fb
      print('Fb is '..fb)
      end
  end
  
  powerState = function()
    if string.find(raw,getPwrStateStandby) then
      print('power return off')
      pwrIsOff()
        else if string.find(raw,getPwrStateOn) then
          pwrIsOn()
          print('power return on')
        end
    end
  end
  
  inputState = function()
    if string.find(raw,inputStateHdmi1) then
      inputIsHdmi1()
        else if string.find(raw,inputStateDp1) then
          inputIsDp1()
        end
    end
  end
  
  
  
  
  sock:Connect(address, port)